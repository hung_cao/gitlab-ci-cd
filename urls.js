module.exports = {
   local: "http://localhost:3000/",
   prod: "https://longhung-test-prod.herokuapp.com/",
   qa: "https://longhung-qa.herokuapp.com/",
};
